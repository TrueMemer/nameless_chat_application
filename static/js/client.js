var socket = io.connect();
var myid, nickname;
var deafened = false;

socket.on('mute user', function(id) {
  if (id !== myid) $(`li#${id} span`).first().toggleClass('label-warning');
});

socket.on('unmute user', function(id) {
  if (id !== myid) $(`li#${id} span`).first().toggleClass('label-warning');
});

socket.on('deafen user', function(id) {
  $(`li#${id} span`).first().toggleClass('label-danger');
});

socket.on('undeafen user', function(id) {
  $(`li#${id} span`).first().toggleClass('label-danger');
});

socket.on('got user', function(user) {
  if (user.id !== myid) $('ul#listaOnline.list-group').append(`<li class="list-group-item" id="${user.id}"><span class="label label-success">●</span> <span id="${user.id}">${user.nickname}</span></li>`);
})

$('#nickname').keydown(function (e) {
    if (e.which == 13) {//13 - это код клавиши "Enter"
        btnEntrar();
    }
})

$('#btnEntrar').on('click', function() {
  btnEntar();
})

function btnEntrar() {
  nickname = $('input#nickname').val();
  $('.nickInput').remove();
  $('#buttons').show();
  initialize();
}

function initialize() {
  if (!easyrtc.supportsGetUserMedia()) {
    $('#status').html("Your browser has no access to the microphone. Please use Google Chrome, Mozilla Firefox or Opera browser.");
    return;
  }

  easyrtc.enableVideo(false);

  // Setup media source
  easyrtc.initMediaSource(function() {
    easyrtc.connect("snacka", function(id) {
      socket.emit('new user', {id: id, nickname: nickname, status: 'available'});
      console.log("Id: " + id);
      myid = id;
      $('ul#listaOnline.list-group').append(`<li class="list-group-item" id="${myid}"><span class="label label-success">●</span> <span id="${myid}">${nickname} (You)</span></li>`);
      updateStatus();
    }, connectFailure);
  }, connectFailure);
  var connectFailure = function(errorCode, errText) {
    $('#status').html(errText);
  }

  // Call everybody
  easyrtc.setRoomOccupantListener(function(room, others) {
    easyrtc.setRoomOccupantListener(false);
    for(var id in others) {
      easyrtc.call(id);
      socket.emit('new user', id);
    }
  });
  $('.status').html('Connected!');
  var clear = setInterval(function() {
    $('.status').hide();
    clearInterval(clear)
  }, 5000)
}

easyrtc.setStreamAcceptor( function(id, stream) {
  $('#videos').append("<video id='" + id + "'></video>");
  socket.emit('get user', id);
  var video = document.getElementById(id);
  easyrtc.setVideoObjectSrc(video, stream);
  updateStatus();
  console.log("Connected: " + id);
});

easyrtc.setOnStreamClosed(function (id) {
  var video = document.getElementById(id);
  easyrtc.setVideoObjectSrc(video, "");
  $(video).remove();
  $('#count').html($('video').length);
  console.log("Disconnected: " + id);
  $(`span.${id}`).remove();
});

function updateStatus() {
  //$('#status').html(status);
}

function muteMicrophone() {
  let isEnabled = easyrtc.getLocalStream().getTracks()[0].enabled;
  if (isEnabled) {
    easyrtc.getLocalStream().getTracks()[0].enabled = !(easyrtc.getLocalStream().getTracks()[0].enabled);
    socket.emit('user muted', myid);
    socket.emit('status changed', {id: myid, status:'muted'});
    $(`li#${myid} span`).first().toggleClass('label-warning');
  } else {
    easyrtc.getLocalStream().getTracks()[0].enabled = true;
    socket.emit('user unmuted', myid); 
    socket.emit('status changed', {id: myid, status:'available'});
    $(`li#${myid} span`).first().toggleClass('label-warning')
  }
}

function deafen() {
  let videos = document.getElementsByTagName('video');
  if (!deafened) {
    Array.prototype.forEach.call(videos, function(el, i){
      el.muted = true;
    });
    deafened = true;
    socket.emit('user deafened', myid);
    socket.emit('status changed', {id: myid, status:'deafened'});
  } else {
    Array.prototype.forEach.call(videos, function(el, i){
      el.muted = false;
    });
    deafened = false;
    socket.emit('user undeafened', myid);
    socket.emit('status changed', {id: myid, status:'available'});
    console.log(myid);
  }
}
