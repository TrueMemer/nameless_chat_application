// Load required modules
var http        = require("http");              // http server core module
var express     = require("express");           // web framework external module
var socket      = require("socket.io");         // web socket external module
var easyrtc     = require("easyrtc");           // EasyRTC external module
var connections = [];

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var app = express();

function requireHTTPS (req, res, next) {
  var secure = req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === "https";
  var local = (/^localhost/).test(req.get('host'));
  if (!secure && !local) {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}
app.use(requireHTTPS);
app.use(express.static(__dirname + "/static/"));

// Start Express http server on port 8080
var port = Number(process.env.PORT || 8080);
var webServer = http.createServer(app).listen(port);

// Start Socket.io so it attaches itself to Express server
var io = socket.listen(webServer, {"log level":1});

io.sockets.on('connection', function (socket) {
  
  socket.on('disconnect', function (data) {
    connections.splice(connections.indexOf(socket), 1);
    console.log('Disconnected!');
  });
  
  socket.on('new user', function(data) {
    connections.push(data);
    console.log(connections);
  });
  
  socket.on('get user', function(id) {
    var user = connections.filter(function(obj) {
      return obj.id === id;
    })[0];
    io.sockets.emit('got user', user);
  });
  
  socket.on('status changed', function (data) {
    io.sockets.emit('change status', {id: data.id, status: data.status});
  });
  
  socket.on('user muted', function (id) {
    io.sockets.emit('mute user', id);
  });

  socket.on('user unmuted', function (id) {
    io.sockets.emit('unmute user', id);
  });
  
  socket.on('user deafened', function (id) {
    io.sockets.emit('deafen user', id);
  });

  socket.on('user undeafened', function (id) {
    io.sockets.emit('undeafen user', id);
  });
});

// Start EasyRTC server
var rtc = easyrtc.listen(app, io);
